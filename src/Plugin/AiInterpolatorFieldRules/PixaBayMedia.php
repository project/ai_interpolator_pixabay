<?php

namespace Drupal\ai_interpolator_pixabay\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\Annotation\AiInterpolatorFieldRule;
use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Utility\Token;
use Drupal\file\FileRepositoryInterface;
use Drupal\media_pixabay\Api\PixabayApiService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The rules for an media field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_pixabay_media",
 *   title = @Translation("Pixabay Image"),
 *   field_rule = "entity_reference",
 *   target = "media"
 * )
 */
class PixaBayMedia extends AiInterpolatorFieldRule implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritDoc}
   */
  public $title = 'Pixabay Image Fetcher';

  /**
   * The media Pixabay API key config.
   */
  protected string $pixabayKeyConfig;

  /**
   * The pixabay api service.
   */
  public PixabayApiService $pixaBay;

  /**
   * The entity type manager.
   */
  public EntityTypeManagerInterface $entityManager;

  /**
   * The File System interface.
   */
  public FileSystemInterface $fileSystem;

  /**
   * The File Repo.
   */
  public FileRepositoryInterface $fileRepo;

  /**
   * The token system to replace and generate paths.
   */
  public Token $token;

  /**
   * The current user.
   */
  public AccountProxyInterface $currentUser;

  /**
   * The logger channel.
   */
  public LoggerChannelFactoryInterface $loggerChannel;

  /**
   * Cache service.
   */
  protected CacheBackendInterface $cache;

  /**
   * The field manager.
   */
  protected EntityFieldManagerInterface $fieldManager;

  /**
   * Construct an image field.
   *
   * @param array $configuration
   *   Inherited configuration.
   * @param string $plugin_id
   *   Inherited plugin id.
   * @param mixed $plugin_definition
   *   Inherited plugin definition.
   * @param \Drupal\media_pixabay\Api\PixabayApiService $pixaBay
   *   The PixaBay API service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityManager
   *   The entity type manager.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The File system interface.
   * @param \Drupal\file\FileRepositoryInterface $fileRepo
   *   The File repo.
   * @param \Drupal\Core\Utility\Token $token
   *   The token system.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannel
   *   The logger channel interface.
   * @param \Drupal\Core\Config\ImmutableConfig $configFactory
   *   The config factory interface.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $fieldManager
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    PixabayApiService $pixaBay,
    EntityTypeManagerInterface $entityManager,
    FileSystemInterface $fileSystem,
    FileRepositoryInterface $fileRepo,
    Token $token,
    AccountProxyInterface $currentUser,
    LoggerChannelFactoryInterface $loggerChannel,
    ImmutableConfig $configFactory,
    CacheBackendInterface $cache,
    EntityFieldManagerInterface $fieldManager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->pixaBay = $pixaBay;
    $this->entityManager = $entityManager;
    $this->fileSystem = $fileSystem;
    $this->fileRepo = $fileRepo;
    $this->token = $token;
    $this->currentUser = $currentUser;
    $this->loggerChannel = $loggerChannel;
    $this->pixabayKeyConfig = $configFactory->get('media_pixabay_api');
    $this->cache = $cache;
    $this->fieldManager = $fieldManager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('media_pixabay.api_service'),
      $container->get('entity_type.manager'),
      $container->get('file_system'),
      $container->get('file.repository'),
      $container->get('token'),
      $container->get('current_user'),
      $container->get('logger.factory'),
      $container->get('config.factory')->get('media_pixabay.admin.config'),
      $container->get('cache.default'),
      $container->get('entity_field.manager'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function needsPrompt() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function advancedMode() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function helpText() {
    return $this->t("Finds images based on a string field search.");
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "";
  }

  /**
   * {@inheritDoc}
   */
  public function extraFormFields(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition) {
    $cardinality = $fieldDefinition->getFieldStorageDefinition()->getCardinality();
    $defaultGeneration = $cardinality < 0 || $cardinality > 10 ? 10 : $cardinality;
    $textAmount = $cardinality == -1 ? 'unlimited' : $cardinality;

    $form['interpolator_media_pixabay_amount'] = [
      '#type' => 'number',
      '#title' => 'Generation Amount',
      '#description' => $this->t('Amount of images to fetch.', [
        '%amount' => $textAmount,
      ]),
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_media_pixabay_amount', $defaultGeneration),
    ];

    $options = [];
    $types = \Drupal::service('entity_type.bundle.info')->getBundleInfo('media');
    foreach ($types as $key => $type) {
      $options[$key] = $type['label'];
    }

    $form['interpolator_media_pixabay_type'] = [
      '#type' => 'select',
      '#title' => 'Media Type',
      '#description' => $this->t('Media Type to set'),
      '#required' => TRUE,
      '#options' => $options,
      '#default_value' => $fieldDefinition->getConfig($entity->bundle())->getThirdPartySetting('ai_interpolator', 'interpolator_media_pixabay_type', 'image'),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    $amount = $interpolatorConfig['media_pixabay_amount'] ?? 1;
    $values = [];
    if (!empty($entity->{$interpolatorConfig['base_field']}->value)) {
      foreach ($entity->get($interpolatorConfig['base_field']) as $wrapperEntity) {
        $searchTerm = $wrapperEntity->value;
        $cid = 'media_pixabay:' . $searchTerm;
        if ($cache = $this->cache->get($cid)) {
          $result = $cache->data;
        }
        else {
          $queryStr = UrlHelper::buildQuery([
            'key' => $this->pixabayKeyConfig,
            'q' => $searchTerm,
            'image_type' => 'image,illustration',
            'per_page' => 100,
          ]);
          $result = $this->pixaBay->apiCall($queryStr);
          // Cache like Pixabay module does.
          $this->cache->set($cid, $result, time() + 24 * 60 * 60, ['pixabay:' . $searchTerm]);
        }
        for ($i = 0; $i < $amount; $i++) {
          // At least Large Image should exists.
          if (!empty($result->hits[$i]->largeImageURL)) {
            $values[] = (array) $result->hits[$i];
          }
        }
      }
    }
    return $values;
  }

  /**
   * {@inheritDoc}
   */
  public function verifyValue(ContentEntityInterface $entity, $value, FieldDefinitionInterface $fieldDefinition) {
    if (is_array($value)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function storeValues(ContentEntityInterface $entity, array $values, FieldDefinitionInterface $fieldDefinition) {
    $mediaStorage = $this->entityManager->getStorage('media');
    $thirdPart = $fieldDefinition->getConfig($entity->bundle())->getThirdPartySettings('ai_interpolator');
    $mediaType = $thirdPart['interpolator_media_pixabay_type'];
    $mediaTypeInterface = $this->entityManager->getStorage('media_type')->load($mediaType);
    $media = $mediaStorage->create([
      'name' => 'hello',
      'bundle' => $mediaType,
    ]);
    $sourceField = $media->getSource()->getSourceFieldDefinition($mediaTypeInterface);
    $fileField = $sourceField->getName();
    $mediaFields = $this->fieldManager->getFieldDefinitions('media', $mediaType);
    $config = $mediaFields[$fileField] ? $mediaFields[$fileField]->getConfig($entity->bundle())->getSettings() : NULL;
    if (!$config) {
      return [];
    }

    // Transform string to boolean.
    $mediaEntitities = [];

    // Successful counter, to only download as many as max.
    $successFul = 0;
    foreach ($values as $value) {
      // Get filename from previewURL.
      $fileName = basename($value['previewURL']) ?? 'created.jpg';
      // Everything validated, then we prepare the file path to save to.
      $filePath = $this->token->replace($config['uri_scheme'] . '://' . rtrim($config['file_directory'], '/')) . '/' . $fileName;
      // Get the right version.
      $fileString = "";
      if (!empty($value['imageURL'])) {
        $fileString = file_get_contents($value['imageURL']);
      }
      elseif (!empty($value['fullHDURL'])) {
        $fileString = file_get_contents($value['fullHDURL']);
      }
      elseif (!empty($value['largeImageURL'])) {
        $fileString = file_get_contents($value['largeImageURL']);
      }
      else {
        return;
      }
      // Create file entity from string.
      $file = $this->generateFileFromString($fileString, $filePath);
      // If we can save, we attach it.
      if ($file) {
        // Get resolution.
        $resolution = getimagesize($file->uri->value);
        // Prepare for Media.
        $fileForMedia = [
          'target_id' => $file->id(),
          'alt' => $config['default_image']['alt'] ?? '',
          'title' => $config['default_image']['title'] ?? '',
          'width' => $resolution[0],
          'height' => $resolution[1],
        ];

        /** @var \Drupal\media\Entity\Media */
        $media = $mediaStorage->create([
          'name' => 'hello',
          'bundle' => $mediaType,
          $fileField => $fileForMedia,
        ]);
        $media->save();

        $mediaEntitities[] = $media->id();

        $successFul++;
        // If we have enough images, give up.
        if ($successFul == $fieldDefinition->getFieldStorageDefinition()->getCardinality()) {
          break;
        }
      }
    }

    // Then set the value.
    $entity->set($fieldDefinition->getName(), $mediaEntitities);
  }

  /**
   * Generate a file entity.
   *
   * @param string $binary
   *   The source binary.
   * @param string $dest
   *   The destination.
   *
   * @return \Drupal\file\FileInterface|false
   *   The file or false on failure.
   */
  private function generateFileFromString(string $binary, string $dest) {
    // Calculate path.
    $fileName = basename($dest);
    $path = substr($dest, 0, -(strlen($fileName) + 1));
    // Create directory if not existsing.
    $this->fileSystem->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY);
    $file = $this->fileRepo->writeData($binary, $dest, FileSystemInterface::EXISTS_RENAME);
    if ($file) {
      return $file;
    }
    return FALSE;
  }

}
